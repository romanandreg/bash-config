#!/usr/bin/env bash

# This loads the nvm configurations
if [[ -d $HOME/.nvm ]] && ! [[ $PATH = *$HOME/.nvm/bin* ]]; then
    export PATH="$HOME/.nvm/bin:$PATH"
    [[ -s "$HOME/.nvm/nvm.sh" ]] && source "$HOME/.nvm/nvm.sh"
fi
