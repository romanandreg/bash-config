#!/bin/bash

### Git Prompt Utilities

# Displays the info of the git repo on the prompt
function perform_git_check {
    local result=$?

    local git_branch
    git_branch=$(git branch 2> /dev/null)

    if [[ -n git_branch ]]; then
        local git_status
        local branch
        local index_files
        local new_files
        local modified_files

        git_status=$(git status 2> /dev/null)
        branch=$(prompt_git_current_branch "$git_branch")
        index_files=$(prompt_git_is_there_files_on_index "$git_status")
        new_files=$(prompt_git_is_there_new_files "$git_status")
        modified_files=$(prompt_git_is_there_modified_files "$git_status")

        echo -ne "${branch}"

        if [[ ! -z "$index_files" ]] || [[ ! -z "$new_files" ]] || [[ ! -z "$modified_files" ]]; then
          echo -ne "${LIGHT_GREEN_FG}(${RESET}${index_files}${modified_files}${new_files}${LIGHT_GREEN_FG})${RESET}"
        fi
    fi

    exit $result
}

# Prints an exclamation (!) whenever
# there is a modified file (not on the index)
function prompt_git_is_there_modified_files {

    git_result=$(echo "$1" | sed -n '/Changes not staged for commit:/p')

    if [[ -n "${git_result}" ]]; then
        printf "%b ✘ %b" "${LIGHT_RED_FG}" "${RESET}"
    fi

    git_result=$(echo "$1" | sed -n '/Changed but not updated:/p')

    if [[ -n "${git_result}" ]]; then
        printf "%b ✘ %b" "${LIGHT_RED_FG}" "${RESET}"
    fi

}

# Prints an star (*) whenever
# there is a modified file (on the index)
function prompt_git_is_there_files_on_index {
    local git_result
    git_result=$(echo "$1" | sed -n '/Changes to be committed:/p')

    if [[ -n "${git_result}" ]]; then
        printf "%b ✔ %b" "${LIGHT_GREEN_FG}" "${RESET}"
    fi
}

# Prints a question mark (?) whenever
# there is a not versioned file on the repo
function prompt_git_is_there_new_files {
    local git_result
    git_result=$(echo "$1" | sed -n '/Untracked files:/p')

    if [[ -n "${git_result}" ]]; then
        printf "%b ? %b" "${LIGHT_YELLOW_FG}" "${RESET}"
    fi
}


# Prints the current git branch on the repo, in
# case there is none, it simply returns an empty string
#
function prompt_git_current_branch {
    local result

    result=$(echo "$1" | sed -e '/^[^*]/d' -e 's/* \(.*\)/- \1 /')
    echo -e "${LIGHT_GREEN_FG}${result}${RESET}"
}

### Last Command utilities

# Returns an error stat code surrounded by brackets
# e.g => [127]
#
# In case there is no error stat, returns an empty
# string
#
function last_command_result {
    local result=$?

    if [[ $result -ne 0 ]]; then
        echo "（╯°□°）╯︵ ┻━┻  [${result}] "
    fi

    exit $result
}

# Returns a color from the previous status code, if
# there was a failure, returns a red color, a green
# otherwise
function fg_color_for_last_command_result {
    local result=$?

    if [[ $result -eq 0 ]]; then
        echo -e "${LIGHT_GREEN_FG}"
    else
        echo -e "${LIGHT_RED_FG}"
    fi

    exit $result
}

function bg_color_for_last_command_result {
    local result=$?

    if [[ $result -eq 0 ]]; then
        echo -e "${LIGHT_GREEN_BG}"
    else
        echo -e "${LIGHT_RED_BG}"
    fi

    exit $result
}

function seconds_to_period {
    local result=$?

    printf " (%dd %dm %ds)" \
           "$(((($1/60)/60)/24))" \
           "$((($1/60)%60))" \
           "$(($1%60))" | \
        sed 's|0d ||;s|0m ||;s| (0s)||'

    exit $result
}

function timer_start {
    SECONDS=0
}

function timer_stop {
    local result="$?"

    printf "%b" "$(seconds_to_period $SECONDS)"

    exit $result
}

trap 'timer_start' DEBUG

function color_for_user {
    local result=$?
    local user

    user=$(whoami)

    if [[ "$user" = 'vagrant' ]] || [[ "$user" = 'ubuntu' ]]; then
        echo -e "${LIGHT_YELLOW_FG}"
    else
        echo -e "${LIGHT_BLUE_FG}"
    fi

    exit $result
}

function get_current_gemset {
    local result=$?
    if [[ $(command -v rvm 2>&1 /dev/null) ]]; then
        local current_gemset
        current_gemset=$(rvm-prompt i g)

        if [[ $(echo "$current_gemset" | grep '@') ]]; then
            printf "%b[%b]%b" "${LIGHT_MAGENTA_FG}" "$current_gemset" "${RESET}"
        fi
    fi
    exit $result
}

function get_current_virtualenv {
    local result=$?
    if [[ -e $VIRTUAL_ENV ]]; then
        local python_virtualenv
        python_virtualenv=$(basename "$VIRTUAL_ENV")

        printf "%b[%b]%b" "${LIGHT_GREEN_FG}" "${python_virtualenv}" "${RESET}"
    fi
    exit $result
}

function print_env {
    local result=$?
    local env_string
    env_string="$(get_current_virtualenv)$(get_current_gemset)"

    local env_string_size=${#env_string}
    if [[ "$env_string_size" -gt 0 ]]; then
        echo -e "${env_string} "
    fi
    exit $result
}

# This prompt should print:
# username@host_name: path_relative_to_home [branch: git branch]
# [error_status_code] $

# Adding \[\] to each color in order to escape invisible characters from the
# PROMPT, that way it won't get fucked up when having long lines or when
# deleting lines using <C-u>. More Info:
# http://www.faqs.org/docs/Linux-HOWTO/Bash-Prompt-HOWTO.html#NONPRINTINGCHARS
# Exception: We are not using this escaping on the git repo info, this is
# because the line they are in doesn't have any input, so we don't have to
# bother about that

export PS1="\n\[\$(bg_color_for_last_command_result)\] \[${RESET}\]\[${LIGHT_BLUE_FG}\]\$(timer_stop)\[$RESET\] \$(print_env)\$(color_for_user)\u@\h$RESET: \[$LIGHT_RED_FG\]\w\[$RESET\] \
\$(perform_git_check)\n\
\[\$(bg_color_for_last_command_result)\] \[$RESET\] \[\$(fg_color_for_last_command_result)\]\$(last_command_result)\$\[$RESET\] "
