#!/bin/sh

# We configure the colors in the
# ls command, the behaviour changes
# depending on the platform
if [ $platform = 'Linux' ]
then
  alias ls='ls --color'
  escape="\e"
elif [ $platform = 'Darwin' ]
then
  # Allow the ls command to have colors when displaying directories
  export CLICOLOR=1
  export LSCOLOR=ExFxCxDxBxegedabagacad
  escape="\033"
fi

# Discover supported colors by executing script found here:
# http://askubuntu.com/questions/27314/script-to-display-all-terminal-colors#answer-279014
export WHITE_UNDERSCORE_FG="${escape}[4;37m"

export LIGHT_RED_FG="${escape}[1;31m"
export LIGHT_RED_BG="${escape}[7;31;40m"

export LIGHT_GREEN_FG="${escape}[1;32m"
export LIGHT_GREEN_BG="${escape}[7;32;40m"

export LIGHT_BLUE_FG="${escape}[5;36;40m"

export LIGHT_BLACK_BG="${escape}[1;40m"

export LIGHT_YELLOW_FG="${escape}[1;33m"

export LIGHT_MAGENTA_FG="${escape}[1;35m"

export LIGHT_BLUE_FG="${escape}[1;34m"

export RESET="${escape}[0m"
