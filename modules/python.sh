#!/bin/bash

export PYENV_HOME="$HOME/.pyenv"
# If GEM_HOME has length eq to zero
# we start with initialization, otherwise
# we skip
if [[ -d $PYENV_HOME ]]; then
    export PATH="$PYENV_HOME/bin:$PATH"
    eval "$(pyenv init -)"
    if [[ -d "$(pyenv root)/plugins/pyenv-virtualenv" ]]; then
        eval "$(pyenv virtualenv-init)"
    fi
fi
