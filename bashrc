#!/usr/bin/env bash

# Terminal will have vi mode when <ESC> is pressed
set -o emacs

# TERM="xterm-256color"

# The OS we are currently using
platform=`uname`

unset PATH
export PATH="/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin"

modules="$HOME/.bash/modules"


## Cloud Services
#
# == Google Cloud Engine app
source $modules/google-cloud.sh
# == EC2 config if available
if [ -e $modules/aws.sh ]
then source $modules/aws.sh
fi

## General
#
# == Nix package manager
source $modules/nix.sh
# == General colors to use on the terminal
source $modules/colors.sh
# == SSH - PGP keychain manager
source $modules/keychain.sh
# == Pimped prompt
source $modules/prompt.sh
# == Handy Aliases
source $modules/aliases.sh
# == Java config
source $modules/java.sh
# == Editor config
source $modules/emacs.sh
# == Git config
source $modules/git.sh
# == Git Completion utility
source $modules/git-completion.sh
# == Haskell config
source $modules/haskell.sh
# == Node.js config
source $modules/nodejs.sh
# == Homebrew config
source $modules/homebrew.sh
# == Home Binaries
source $modules/localbin.sh
# == Heroku config
source $modules/heroku.sh
# == Virtualenv config
source $modules/virtualenv.sh
# == Hitch (Pair Programming)
source $modules/hitch.sh
# == Ruby config
source $modules/ruby.sh
# == PyEnv settings
source $modules/pyenv.sh
# == RVM config
source $modules/rvm.sh
# == NVM config
source $modules/nvm.sh

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
  # Shell is non-interactive.  Be done now!
  return
fi

unset PROMPT_COMMAND
